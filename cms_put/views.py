from django.shortcuts import render

from django.http import HttpResponse, Http404

from django.views.decorators.csrf import csrf_exempt

from .models import Contenido

from django.template import loader #permite manejar el template

formulario = """
<form action="" method="POST">
    <input type="contenido" name="contenido" id="contenido">
    <input type="submit" values="Enviar">
</form>

"""
# Create your views here.

@csrf_exempt#Decorador para quitar el control de seguridad de CSRF
def get_resources(request, recurso):

    if request.method == "PUT":
        valor = request.POST["valor"]
        c = Contenido(clave=recurso, valor=valor)
        c.save()

    try:
        contenido = Contenido.objects.get(clave=recurso)
        respuesta = "El recurso pedido es: "+ contenido.clave + ", con valor: "+ contenido.valor + "Cuyo identificador es: "+str(contenido.id)
    except Contenido.DoesNotExist:
        respuesta = "El recurso no existe" + formulario

    return HttpResponse(respuesta)

def index(request):
    return HttpResponse("Pagina principal de cms_put")